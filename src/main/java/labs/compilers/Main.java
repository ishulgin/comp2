package labs.compilers;

import labs.compilers.core.LexemesExtractor;
import labs.compilers.model.ResultContainer;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = getBufferedReader(args)) {
            ResultContainer resultContainer = LexemesExtractor.extractLexemes(reader);

            resultContainer.printResult();
        }
    }

    private static BufferedReader getBufferedReader(String[] args) throws FileNotFoundException {
        Reader baseReader = args.length == 0 ?
                new InputStreamReader(System.in) :
                new FileReader(args[0]);

        return new BufferedReader(baseReader);
    }
}
