package labs.compilers.core;

import labs.compilers.model.*;
import labs.compilers.model.ast.NonTerminalNode;
import labs.compilers.model.ast.TerminalNode;

import java.util.*;
import java.util.stream.Collectors;

public class Analyser {
    private static final Map<TablePair, List<AnalyserUnit>> LL_TABLE = new HashMap<>();

    private final Deque<AnalyserUnit> unitsLeft = new ArrayDeque<>();

    private NonTerminalNode root;

    private NonTerminalNode currentNode;

    private boolean isFailed = false;

    private String error;

    static {
        LL_TABLE.put(TablePair.of(NonTerminal.PROGRAM, LexemeType.VAR), List.of(NonTerminal.DECL, NonTerminal.EVAL_1));

        LL_TABLE.put(TablePair.of(NonTerminal.EVAL_1, LexemeType.IDENTIFIER), List.of(NonTerminal.OP, NonTerminal.EVAL_2));
        LL_TABLE.put(TablePair.of(NonTerminal.EVAL_1, LexemeType.IF), List.of(NonTerminal.OP, NonTerminal.EVAL_2));
        LL_TABLE.put(TablePair.of(NonTerminal.EVAL_2, LexemeType.IDENTIFIER), List.of(NonTerminal.EVAL_1));
        LL_TABLE.put(TablePair.of(NonTerminal.EVAL_2, LexemeType.IF), List.of(NonTerminal.EVAL_1));

        LL_TABLE.put(TablePair.of(NonTerminal.DECL, LexemeType.VAR), List.of(LexemeType.VAR, NonTerminal.VAR_LIST_1, LexemeType.SEMICOLON));

        LL_TABLE.put(TablePair.of(NonTerminal.VAR_LIST_1, LexemeType.IDENTIFIER), List.of(LexemeType.IDENTIFIER, NonTerminal.VAR_LIST_2));
        LL_TABLE.put(TablePair.of(NonTerminal.VAR_LIST_2, LexemeType.SEMICOLON), Collections.emptyList());

        LL_TABLE.put(TablePair.of(NonTerminal.VAR_LIST_2, LexemeType.COMMA), List.of(LexemeType.COMMA, NonTerminal.VAR_LIST_1));

        LL_TABLE.put(TablePair.of(NonTerminal.OP, LexemeType.IDENTIFIER), List.of(NonTerminal.ASSIGNMENT));
        LL_TABLE.put(TablePair.of(NonTerminal.OP, LexemeType.IF), List.of(NonTerminal.COMPL_OP_1));

        LL_TABLE.put(TablePair.of(NonTerminal.ASSIGNMENT, LexemeType.IDENTIFIER), List.of(LexemeType.IDENTIFIER, LexemeType.ASSIGNMENT, NonTerminal.EXPR_1, LexemeType.SEMICOLON));

        LL_TABLE.put(TablePair.of(NonTerminal.EXPR_1, LexemeType.IDENTIFIER), List.of(NonTerminal.EXPR_3, NonTerminal.EXPR_2));
        LL_TABLE.put(TablePair.of(NonTerminal.EXPR_1, LexemeType.O_BRACKET), List.of(NonTerminal.EXPR_3, NonTerminal.EXPR_2));
        LL_TABLE.put(TablePair.of(NonTerminal.EXPR_1, LexemeType.CONSTANT), List.of(NonTerminal.EXPR_3, NonTerminal.EXPR_2));
        LL_TABLE.put(TablePair.of(NonTerminal.EXPR_2, LexemeType.OPERATOR), List.of(LexemeType.OPERATOR, NonTerminal.EXPR_1));
        LL_TABLE.put(TablePair.of(NonTerminal.EXPR_2, LexemeType.SEMICOLON), Collections.emptyList());
        LL_TABLE.put(TablePair.of(NonTerminal.EXPR_2, LexemeType.THEN), Collections.emptyList());
        LL_TABLE.put(TablePair.of(NonTerminal.EXPR_2, LexemeType.C_BRACKET), Collections.emptyList());
        LL_TABLE.put(TablePair.of(NonTerminal.EXPR_3, LexemeType.IDENTIFIER), List.of(NonTerminal.OPER));
        LL_TABLE.put(TablePair.of(NonTerminal.EXPR_3, LexemeType.O_BRACKET), List.of(LexemeType.O_BRACKET, NonTerminal.EXPR_1, LexemeType.C_BRACKET));
        LL_TABLE.put(TablePair.of(NonTerminal.EXPR_3, LexemeType.CONSTANT), List.of(NonTerminal.OPER));

        LL_TABLE.put(TablePair.of(NonTerminal.OPER, LexemeType.IDENTIFIER), List.of(LexemeType.IDENTIFIER));
        LL_TABLE.put(TablePair.of(NonTerminal.OPER, LexemeType.CONSTANT), List.of(LexemeType.CONSTANT));

        LL_TABLE.put(TablePair.of(NonTerminal.COMPL_OP_1, LexemeType.IF), List.of(LexemeType.IF, NonTerminal.EXPR_1, LexemeType.THEN, NonTerminal.OP, NonTerminal.COMPL_OP_2));
        LL_TABLE.put(TablePair.of(NonTerminal.COMPL_OP_2, LexemeType.IDENTIFIER), Collections.emptyList());
        LL_TABLE.put(TablePair.of(NonTerminal.COMPL_OP_2, LexemeType.ELSE), List.of(LexemeType.ELSE, NonTerminal.OP));
    }

    public Analyser() {
        unitsLeft.push(NonTerminal.PROGRAM);
    }

    public String getTree() {
        assert root != null;

        return root.getDeepString("");
    }

    public String getError() {
        assert isFailed;

        return error;
    }

    public boolean isIncomplete() {
        return unitsLeft.size() != 1 || unitsLeft.iterator().next() != NonTerminal.EVAL_2;
    }

    public boolean appendNode(Lexeme lexeme) {
        assert !isFailed;

        AnalyserUnit peek = unitsLeft.pop();

        if (peek instanceof LexemeType) {
            return processTerminal(lexeme, (LexemeType) peek);
        } else {
            List<AnalyserUnit> analyserUnits = LL_TABLE.get(TablePair.of((NonTerminal) peek, lexeme.getType()));

            if (analyserUnits == null) {
                appendErrorNode(lexeme, peek);

                return false;
            }

            refreshCurrentNode();

            if (analyserUnits.isEmpty()) {
                currentNode.addEmptyChild();
            } else {
                currentNode = NonTerminalNode.of((NonTerminal) peek, currentNode, analyserUnits.size());
                if (root == null) {
                    root = currentNode;
                }

                pushRule(analyserUnits);
            }

            return appendNode(lexeme);
        }
    }

    private boolean processTerminal(Lexeme lexeme, LexemeType peek) {
        refreshCurrentNode();

        if (peek != lexeme.getType()) {
            appendErrorNode(lexeme, peek);

            return false;
        }

        currentNode.appendChild(new TerminalNode(lexeme));

        return true;
    }

    private void refreshCurrentNode() {
        if (currentNode != null) {
            while (currentNode.isProcessed()) {
                currentNode = currentNode.getParent();
            }
        }
    }

    private void pushRule(List<AnalyserUnit> analyserUnits) {
        ListIterator<AnalyserUnit> iterator = analyserUnits.listIterator(analyserUnits.size());
        while (iterator.hasPrevious()) {
            unitsLeft.push(iterator.previous());
        }
    }

    private void appendErrorNode(Lexeme lexeme, AnalyserUnit peek) {
        isFailed = true;

        String expected;
        if (peek instanceof LexemeType) {
            expected = peek.toString();
        } else {
            expected = LL_TABLE
                    .keySet()
                    .stream()
                    .filter(k -> k.getNonTerminal() == peek)
                    .map(TablePair::getTerminal)
                    .map(Enum::toString)
                    .collect(Collectors.joining(", ", "one of [", "]"));

        }
        error = String.format("Error(%d, %d): Expected %s, got %s", lexeme.getLine(), lexeme.getStartsAt(),
                expected, lexeme.getType());

        NonTerminalNode.of(NonTerminal.ERROR, currentNode, 0);
    }
}
