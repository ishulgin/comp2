package labs.compilers.core;

import labs.compilers.model.Lexeme;
import labs.compilers.model.LexemeType;
import labs.compilers.model.ResultContainer;

import java.io.BufferedReader;
import java.io.IOException;

import static labs.compilers.model.LexemeType.UNKNOWN;

public final class LexemesExtractor {
    private LexemesExtractor() {
    }

    public static ResultContainer extractLexemes(BufferedReader reader) throws IOException {
        ResultContainer resultContainer = new ResultContainer();
        ReadDataHolder readDataHolder = new ReadDataHolder();

        String line;
        while ((line = reader.readLine()) != null) {
            processLine(resultContainer, readDataHolder, line);

            readDataHolder.reset();
        }

        return resultContainer;
    }

    private static void processLine(ResultContainer resultContainer, ReadDataHolder readDataHolder, String line) {
        Lexeme previousGreedyLexeme = null;
        char[] charArray = line.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            char character = charArray[i];
            if (isComment(charArray, i, character)) {
                break;
            }

            readDataHolder.buf += character;
            LexemeType lexemeType = TypeParser.parseType(readDataHolder.buf);

            if (shouldUpdatePreviousGreedy(previousGreedyLexeme, lexemeType)) {
                previousGreedyLexeme.appendValue(character);
                // На случай, если тип лексемы поменялся (пусть и нерелевантно для текущей грамматики)
                previousGreedyLexeme.updateLexemeType(lexemeType);
            } else {
                previousGreedyLexeme =
                        handleNewLexeme(readDataHolder, resultContainer, previousGreedyLexeme, i, lexemeType);
            }
        }

        if (previousGreedyLexeme != null) {
            resultContainer.appendLexeme(previousGreedyLexeme);
        } else if (isPreviousUnknownLexeme(readDataHolder)) {
            resultContainer.setLexemeError(true);

            System.err.printf("Error(%d, %d): Unknown lexeme \"%s\".%n", readDataHolder.lineNumber,
                    readDataHolder.linePosition, readDataHolder.buf);
        }
    }

    private static boolean isComment(char[] charArray, int i, char character) {
        return character == '/' && i != charArray.length - 1 && charArray[i + 1] == '/';
    }

    private static boolean shouldUpdatePreviousGreedy(Lexeme previousGreedyLexeme, LexemeType lexemeType) {
        return previousGreedyLexeme != null && lexemeType != UNKNOWN;
    }

    private static Lexeme handleNewLexeme(ReadDataHolder readDataHolder, ResultContainer resultContainer,
                                          Lexeme previousGreedyLexeme, int i, LexemeType lexemeType) {
        Lexeme newPreviousGreedyLexeme = null;

        if (previousGreedyLexeme != null) {
            resultContainer.appendLexeme(previousGreedyLexeme);

            readDataHolder.linePosition = i + 1;

            readDataHolder.setBufToLastChar();
            lexemeType = TypeParser.parseType(readDataHolder.buf);
        }

        if (lexemeType != UNKNOWN) {
            Lexeme lexeme = Lexeme.of(readDataHolder.lineNumber, readDataHolder.linePosition,
                    readDataHolder.buf, lexemeType);
            if (lexemeType.isGreedy()) {
                newPreviousGreedyLexeme = lexeme;
            } else {
                resultContainer.appendLexeme(lexeme);

                readDataHolder.linePosition = i + 2;
                readDataHolder.resetBuf();
            }
        }

        return newPreviousGreedyLexeme;
    }

    private static boolean isPreviousUnknownLexeme(ReadDataHolder readDataHolder) {
        return readDataHolder.buf.length() != 0;
    }

    private static class ReadDataHolder {
        private String buf = "";
        private int linePosition = 1;
        private int lineNumber = 1;

        private void reset() {
            buf = "";
            linePosition = 1;
            lineNumber++;
        }

        private void resetBuf() {
            buf = "";
        }

        private void setBufToLastChar() {
            assert buf.length() > 0;

            buf = String.valueOf(buf.charAt(buf.length() - 1));
        }
    }
}