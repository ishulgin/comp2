package labs.compilers.core;

import labs.compilers.model.LexemeType;
import labs.compilers.util.MultiKeyMapBuilder;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static labs.compilers.model.LexemeType.*;

final class TypeParser {
    private static final Map<String, LexemeType> SIMPLE_LEXEMES_CRITERIA =
            new MultiKeyMapBuilder<String, LexemeType>()
                    .put(ASSIGNMENT, ":=")
                    .put(VAR, "Var")
                    .put(IF, "IF")
                    .put(THEN, "THEN")
                    .put(ELSE, "ELSE")
                    .put(O_BRACKET, "(")
                    .put(C_BRACKET, ")")
                    .put(COMMA, ",")
                    .put(SEMICOLON, ";")
                    .put(OPERATOR, "-", "+", "*", "/", "<", ">", "==")
                    .build();

    private static final List<ComplexLexemePair> COMPLEX_LEXEMES_CRITERIA = List.of(
            ComplexLexemePair.of(SEPARATOR, str -> str.matches("\\s+")),
            ComplexLexemePair.of(CONSTANT, str -> str.matches("\\d+")),
            ComplexLexemePair.of(IDENTIFIER, str -> str.matches("[A-Za-z]+"))
    );

    private TypeParser() {
    }

    static LexemeType parseType(String str) {
        LexemeType result = SIMPLE_LEXEMES_CRITERIA.get(str);
        if (result != null) {
            return result;
        }

        return COMPLEX_LEXEMES_CRITERIA
                .stream()
                .filter(it -> it.criteria.test(str))
                .map(it -> it.lexemeType)
                .findFirst()
                .orElse(UNKNOWN);
    }

    @AllArgsConstructor
    private static class ComplexLexemePair {
        private final LexemeType lexemeType;
        private final Predicate<String> criteria;

        private static ComplexLexemePair of(LexemeType lexemeType, Predicate<String> criteria) {
            return new ComplexLexemePair(lexemeType, criteria);
        }
    }
}
