package labs.compilers.model;

import lombok.*;

@Getter
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
public class Constant {
    private final String hexValue;

    public static Constant fromString(String string) {
        return new Constant("0x" + Integer.toHexString(Integer.parseInt(string)));
    }
}
