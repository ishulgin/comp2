package labs.compilers.model;

import lombok.Data;

@Data
public class Identifier {
    private final String value;
}
