package labs.compilers.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Lexeme implements AnalyserUnit {
    private final int line;
    private final int startsAt;
    private String value;
    private LexemeType type;

    Lexeme(Lexeme lexeme) {
        line = lexeme.line;
        startsAt = lexeme.startsAt;
        value = lexeme.value;
        type = lexeme.type;
    }

    public static Lexeme of(int line, int startsAt, String value, LexemeType lexemeType) {
        return new Lexeme(line, startsAt, value, lexemeType);
    }

    public void appendValue(char c) {
        value += c;
    }

    public void updateLexemeType(LexemeType newType) {
        type = newType;
    }

    @Override
    public String toString() {
        return String.format("%s(%d, %d): \"%s\".", type, line, startsAt, value);
    }
}
