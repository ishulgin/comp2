package labs.compilers.model;

public enum LexemeType implements AnalyserUnit {
    ASSIGNMENT(false),
    OPERATOR(false),
    SEPARATOR(true),
    VAR(false),
    IF(false),
    THEN(false),
    ELSE(false),
    O_BRACKET(false),
    C_BRACKET(false),
    COMMA(false),
    SEMICOLON(false),
    CONSTANT(true),
    IDENTIFIER(true),
    UNKNOWN(true);

    private final boolean greedy;

    LexemeType(boolean greedy) {
        this.greedy = greedy;
    }

    public boolean isGreedy() {
        return greedy;
    }
}
