package labs.compilers.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@EqualsAndHashCode
public class LexemesTable {
    private final List<Lexeme> lexemes = new ArrayList<>();
    private final Map<String, Constant> constants = new HashMap<>();
    private final Map<String, Identifier> identifiers = new HashMap<>();

    Lexeme appendLexeme(Lexeme lexeme) {
        Lexeme valueToAdd;

        switch (lexeme.getType()) {
            case CONSTANT:
                Constant constant = constants.get(lexeme.getValue());
                if (constant == null) {
                    constant = Constant.fromString(lexeme.getValue());
                    constants.put(lexeme.getValue(), constant);
                }

                valueToAdd = ReferencedLexeme.wrap(lexeme, constant);
                break;
            case IDENTIFIER:
                Identifier identifier = identifiers.get(lexeme.getValue());
                if (identifier == null) {
                    identifier = new Identifier(lexeme.getValue());
                    identifiers.put(lexeme.getValue(), identifier);
                }

                valueToAdd = ReferencedLexeme.wrap(lexeme, identifier);
                break;
            default:
                valueToAdd = lexeme;
        }

        lexemes.add(valueToAdd);

        return valueToAdd;
    }
}
