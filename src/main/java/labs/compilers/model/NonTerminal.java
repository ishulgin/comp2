package labs.compilers.model;

public enum NonTerminal implements AnalyserUnit {
    PROGRAM,
    EVAL_1,
    EVAL_2,
    DECL,
    VAR_LIST_1,
    VAR_LIST_2,
    OP,
    ASSIGNMENT,
    EXPR_1,
    EXPR_2,
    EXPR_3,
    OPER,
    COMPL_OP_1,
    COMPL_OP_2,
    ERROR
}
