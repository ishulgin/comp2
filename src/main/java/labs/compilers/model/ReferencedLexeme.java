package labs.compilers.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(callSuper = true)
public class ReferencedLexeme<V> extends Lexeme {
    private final V reference;

    private ReferencedLexeme(Lexeme lexeme, V reference) {
        super(lexeme);

        this.reference = reference;
    }

    public static <V> ReferencedLexeme<V> wrap(Lexeme lexeme, V reference) {
        return new ReferencedLexeme<>(lexeme, reference);
    }

    @Override
    public String toString() {
        return super.toString() + " -> " + reference.toString();
    }
}
