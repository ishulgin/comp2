package labs.compilers.model;

import labs.compilers.core.Analyser;
import lombok.Getter;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ResultContainer {
    @Getter
    private final LexemesTable lexemesTable = new LexemesTable();

    private final Analyser analyser = new Analyser();

    private boolean isAnalyserError;

    private boolean isLexemeError;

    public void appendLexeme(Lexeme lexeme) {
        Lexeme lexemeToAppend = lexemesTable.appendLexeme(lexeme);

        if (!isLexemeError && !isAnalyserError && lexeme.getType() != LexemeType.SEPARATOR) {
            isAnalyserError = !analyser.appendNode(lexemeToAppend);
        }
    }

    public void printResult() {
        String lexemes = mapAndJoin(lexemesTable.getLexemes(), Lexeme::toString);
        String constants = mapAndJoin(lexemesTable.getConstants().values(), Constant::getHexValue);
        String identifiers = mapAndJoin(lexemesTable.getIdentifiers().values(), Identifier::getValue);

        System.out.println("# LEXEMES");
        System.out.println(lexemes + "\n");

        System.out.println("# CONSTANTS");
        System.out.println(constants + "\n");

        System.out.println("# IDENTIFIERS");
        System.out.println(identifiers + "\n");

        System.out.println("# AST");
        if (isLexemeError) {
            System.err.println("Error: Tree wasn't completely built.");
        }

        if (isAnalyserError) {
            System.err.println(analyser.getError());
        } else if (analyser.isIncomplete()) {
            System.err.println("Error: Premature end of lexemes stream.");
        }

        System.out.println(analyser.getTree());
    }

    public void setLexemeError(boolean lexemeError) {
        isLexemeError = lexemeError;
    }

    private static <T> String mapAndJoin(Collection<T> collection, Function<T, String> function) {
        return collection
                .stream()
                .map(function)
                .collect(Collectors.joining("\n"));
    }
}
