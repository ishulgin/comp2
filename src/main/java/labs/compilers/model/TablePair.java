package labs.compilers.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class TablePair {
    private final NonTerminal nonTerminal;
    private final LexemeType terminal;

    public static TablePair of(NonTerminal nonTerminal, LexemeType terminal) {
        return new TablePair(nonTerminal, terminal);
    }
}
