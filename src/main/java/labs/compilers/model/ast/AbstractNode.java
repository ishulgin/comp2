package labs.compilers.model.ast;

import labs.compilers.model.AnalyserUnit;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
public abstract class AbstractNode {
    static final String MARKER = "|-";

    final AnalyserUnit type;

    public AnalyserUnit getType() {
        return type;
    }

    public abstract String getDeepString(String baseMarker);
}
