package labs.compilers.model.ast;

import labs.compilers.model.NonTerminal;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class NonTerminalNode extends AbstractNode {
    private final NonTerminalNode parent;

    private final List<AbstractNode> children;

    private int capacity;

    private NonTerminalNode(NonTerminal type, NonTerminalNode parent, int capacity) {
        super(type);
        this.parent = parent;
        this.capacity = capacity;

        this.children = new ArrayList<>(capacity);

        if (parent != null) {
            parent.appendChild(this);
        }
    }

    public static NonTerminalNode of(NonTerminal type, NonTerminalNode parent, int capacity) {
        return new NonTerminalNode(type, parent, capacity);
    }

    public void addEmptyChild() {
        assert capacity > children.size();
        capacity--;
    }

    public void appendChild(AbstractNode child) {
        addEmptyChild();

        children.add(child);
    }

    public boolean isProcessed() {
        return capacity == 0;
    }

    public NonTerminalNode getParent() {
        return parent;
    }

    @Override
    public String getDeepString(String baseMarker) {
        return baseMarker + type + (
                children.isEmpty() ?
                        "" :
                        '\n' + children
                                .stream()
                                .map(it -> it.getDeepString(baseMarker + MARKER))
                                .collect(Collectors.joining("\n")));
    }
}
