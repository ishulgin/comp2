package labs.compilers.model.ast;

import labs.compilers.model.Lexeme;

public class TerminalNode extends AbstractNode {
    private final Lexeme value;

    public TerminalNode(Lexeme value) {
        super(value.getType());
        this.value = value;
    }

    @Override
    public String getDeepString(String baseMarker) {
        return baseMarker + value;
    }
}
