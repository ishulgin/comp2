package labs.compilers.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MultiKeyMapBuilder<K, V> {
    private final Map<K, V> map = new HashMap<>();

    @SafeVarargs
    public final MultiKeyMapBuilder<K, V> put(V value, K... keys) {
        Arrays.stream(keys).forEach(it -> map.put(it, value));

        return this;
    }

    public Map<K, V> build() {
        return map;
    }
}
