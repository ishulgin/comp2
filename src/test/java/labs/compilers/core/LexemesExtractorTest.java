package labs.compilers.core;

import labs.compilers.model.*;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static labs.compilers.model.LexemeType.*;

class LexemesExtractorTest {
    @Test
    void testExtractLexemes() {
        var testData = List.of(
                TestData.of("comment.txt", Collections.emptyList(), Collections.emptyMap(), Collections.emptyMap()),
                getVariablesDeclaration(),
                getAssignment(),
                getIf()
        );

        Assertions.assertAll(
                testData
                        .stream()
                        .peek(it -> {
                            try (var reader = new BufferedReader(
                                    new InputStreamReader(getClass().getClassLoader().getResourceAsStream(it.fileName)))) {
                                it.actual = LexemesExtractor.extractLexemes(reader);
                            } catch (IOException e) {
                                throw new IllegalStateException(e);
                            }
                        })
                        .map(it -> (Executable) () -> {
                            Assertions.assertEquals(it.expectedLexemes, it.actual.getLexemesTable().getLexemes());
                            Assertions.assertEquals(it.expectedIdentifiers, it.actual.getLexemesTable().getIdentifiers());
                            Assertions.assertEquals(it.expectedConstants, it.actual.getLexemesTable().getConstants());
                        })
        );
    }

    private static TestData getVariablesDeclaration() {
        String aaa = "aaa";
        Identifier identifierAaa = new Identifier(aaa);
        String b = "b";
        Identifier identifierB = new Identifier(b);
        String c = "c";
        Identifier identifierC = new Identifier(c);

        return TestData.of("variablesDeclaration.txt", List.of(
                Lexeme.of(1, 1, "Var", VAR),
                Lexeme.of(1, 4, " ", SEPARATOR),
                ReferencedLexeme.wrap(Lexeme.of(1, 5, aaa, IDENTIFIER), identifierAaa),
                Lexeme.of(1, 8, ",", COMMA),
                ReferencedLexeme.wrap(Lexeme.of(1, 9, "b", IDENTIFIER), identifierB),
                Lexeme.of(1, 10, ",", COMMA),
                Lexeme.of(1, 11, "   ", SEPARATOR),
                ReferencedLexeme.wrap(Lexeme.of(1, 14, c, IDENTIFIER), identifierC),
                Lexeme.of(1, 15, ";", SEMICOLON)
        ), Map.of(aaa, identifierAaa, b, identifierB, c, identifierC), Collections.emptyMap());
    }

    private static TestData getAssignment() {
        String a = "a";
        Identifier identifierA = new Identifier(a);
        String const1 = "1";
        Constant constant1 = Constant.fromString(const1);
        String b = "b";
        Identifier identifierB = new Identifier(b);
        String const2 = "2";
        Constant constant2 = Constant.fromString(const2);

        return TestData.of("assignment.txt", List.of(
                ReferencedLexeme.wrap(Lexeme.of(1, 1, a, IDENTIFIER), identifierA),
                Lexeme.of(1, 2, ":=", ASSIGNMENT),
                ReferencedLexeme.wrap(Lexeme.of(1, 4, const1, CONSTANT), constant1),
                Lexeme.of(1, 5, ";", SEMICOLON),

                ReferencedLexeme.wrap(Lexeme.of(2, 1, b, IDENTIFIER), identifierB),
                Lexeme.of(2, 2, " ", SEPARATOR),
                Lexeme.of(2, 3, ":=", ASSIGNMENT),
                Lexeme.of(2, 5, " ", SEPARATOR),
                Lexeme.of(2, 6, "(", O_BRACKET),
                ReferencedLexeme.wrap(Lexeme.of(2, 7, a, IDENTIFIER), identifierA),
                Lexeme.of(2, 8, " ", SEPARATOR),
                Lexeme.of(2, 9, "-", OPERATOR),
                Lexeme.of(2, 10, " ", SEPARATOR),
                ReferencedLexeme.wrap(Lexeme.of(2, 11, const2, CONSTANT), constant2),
                Lexeme.of(2, 12, ")", C_BRACKET),
                Lexeme.of(2, 13, " ", SEPARATOR),
                Lexeme.of(2, 14, ";", SEMICOLON)
        ), Map.of(a, identifierA, b, identifierB), Map.of(const1, constant1, const2, constant2));
    }

    private static TestData getIf() {
        String a = "a";
        Identifier identifierA = new Identifier(a);
        String b = "b";
        Identifier identifierB = new Identifier(b);
        String const5 = "5";
        Constant constant5 = Constant.fromString(const5);
        String const4 = "4";
        Constant constant4 = Constant.fromString(const4);

        return TestData.of("if.txt", List.of(
                Lexeme.of(1, 1, "IF", IF),
                Lexeme.of(1, 3, " ", SEPARATOR),
                ReferencedLexeme.wrap(Lexeme.of(1, 4, a, IDENTIFIER), identifierA),
                Lexeme.of(1, 5, " ", SEPARATOR),
                Lexeme.of(1, 6, "THEN", THEN),

                Lexeme.of(3, 1, "    ", SEPARATOR),
                ReferencedLexeme.wrap(Lexeme.of(3, 5, a, IDENTIFIER), identifierA),
                Lexeme.of(3, 6, " ", SEPARATOR),
                Lexeme.of(3, 7, ":=", ASSIGNMENT),
                Lexeme.of(3, 9, " ", SEPARATOR),
                ReferencedLexeme.wrap(Lexeme.of(3, 10, const5, CONSTANT), constant5),
                Lexeme.of(3, 11, ";", SEMICOLON),

                Lexeme.of(4, 1, "ELSE", ELSE),

                Lexeme.of(5, 1, "    ", SEPARATOR),

                Lexeme.of(6, 1, "    ", SEPARATOR),
                ReferencedLexeme.wrap(Lexeme.of(6, 5, b, IDENTIFIER), identifierB),
                Lexeme.of(6, 6, ":=", ASSIGNMENT),
                ReferencedLexeme.wrap(Lexeme.of(6, 8, const4, CONSTANT), constant4),
                Lexeme.of(6, 9, ";", SEMICOLON)
        ), Map.of(a, identifierA, b, identifierB), Map.of(const5, constant5, const4, constant4));
    }

    @RequiredArgsConstructor
    private static class TestData {

        private final String fileName;
        private final List<Lexeme> expectedLexemes;
        private final Map<String, Identifier> expectedIdentifiers;
        private final Map<String, Constant> expectedConstants;
        private ResultContainer actual;

        private static TestData of(String string, List<Lexeme> expectedLexemes, Map<String, Identifier> expectedIdentifiers, Map<String, Constant> expectedConstants) {
            return new TestData(string, expectedLexemes, expectedIdentifiers, expectedConstants);
        }
    }
}
