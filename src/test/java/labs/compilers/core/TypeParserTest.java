package labs.compilers.core;

import labs.compilers.model.LexemeType;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.List;

import static labs.compilers.model.LexemeType.*;

class TypeParserTest {
    @Test
    void parseType() {
        var testData = List.of(
                TestData.of("  \t", SEPARATOR),
                TestData.of("asd", IDENTIFIER),
                TestData.of("123", CONSTANT),
                TestData.of("(", C_BRACKET),
                TestData.of(":=", ASSIGNMENT),
                TestData.of("THEN", THEN),
                TestData.of("/", OPERATOR)
        );

        Assertions.assertAll(
                testData
                        .stream()
                        .peek(it -> it.actual = TypeParser.parseType(it.string))
                        .map(it -> (Executable) () -> Assertions.assertEquals(it.expected, it.actual))
        );
    }

    @RequiredArgsConstructor
    private static class TestData {
        private final String string;
        private final LexemeType expected;
        private LexemeType actual;

        private static TestData of(String string, LexemeType expected) {
            return new TestData(string, expected);
        }
    }
}